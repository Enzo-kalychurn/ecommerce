const express = require('express');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const app = express();

app.use(cookieParser());


app.use(session({
  secret: 'dragon',
  resave: false,
  saveUninitialized: false,
}));
